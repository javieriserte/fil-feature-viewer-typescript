const HtmlWebpackPlugin = require('html-webpack-plugin');
const common = require('./webpack.config');
const { merge } = require('webpack-merge');

module.exports = merge(common, {
  mode: 'production',
  output: {
    filename: '[name].[contenthash].js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Fil Feature Viewer',
      filename: 'index.[contenthash].html',
      template: 'assets/index.html',
      scriptLoading:'blocking',
      inject: 'head'
    }),
  ]
});