const common = require('./webpack.config');
const { merge } = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
module.exports = merge(common, {
  mode: 'development',
  output: {
    filename: '[name].js',
  },
  devtool: false,
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Fil Feature Viewer',
      filename: 'index.html',
      template: 'assets/index.html',
      scriptLoading:'blocking',
      inject: 'head'
    }),
    new webpack.SourceMapDevToolPlugin({})
  ]
});